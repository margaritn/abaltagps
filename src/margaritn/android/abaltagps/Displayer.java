package margaritn.android.abaltagps;

import android.view.View;
import android.widget.TextView;
import android.util.Log;
import android.location.Location;

// class managing displayning location data in 'text format' /using simple TextView widget
public class Displayer {
	protected TextView vDisplay;
	protected String mode;
	
	public Displayer(TextView v, String m) {
		vDisplay = v;
		mode = m; // mode - recording or playback
	}
	
	// display location in textview widget
	public void setLocation(Location loc) {
		vDisplay.setText(
			MainActivity.i18n(R.string.statusText) + mode + "\n" +
			MainActivity.i18n(R.string.longtitudeText) + String.valueOf(loc.getLongitude()) + "\n" +
			MainActivity.i18n(R.string.latitudeText) + String.valueOf(loc.getLatitude()) + "\n" +
			MainActivity.i18n(R.string.headingText) + String.valueOf(loc.getBearing()) + 
				" " + MainActivity.i18n(R.string.degreesText) + "\n" +
			MainActivity.i18n(R.string.speedText) + String.valueOf(loc.getSpeed()) + 
				" " + MainActivity.i18n(R.string.metersPerSecondText) + "\n" +
			MainActivity.i18n(R.string.accuracyText) + String.valueOf(loc.getAccuracy()) + "\n"
		);
	}
	
	// might be used to show some status, e.g. 'waiting for fix'
	public void setStatus(String status) {
		vDisplay.setText(MainActivity.i18n(R.string.statusText) + status);
	}	
}
