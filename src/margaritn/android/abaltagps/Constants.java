package margaritn.android.abaltagps;

import android.location.LocationManager;

public class Constants
{
	public static final String TAG = "abaltagps";
	public static final String LOG_FILE_NAME = "GPS_data.log";
	public static final String FILE_FIELDS_DELIMITER = ",";
	public static final long RECORDING_PERIOD = 5*1000; // in milliseconds
	public static final long PLAYBACK_PERIOD = 1*1000; // in milliseconds
	public static final String LOCATION_PROVIDER = LocationManager.GPS_PROVIDER;
	// public static final String LOCATION_PROVIDER = LocationManager.NETWORK_PROVIDER;	
}
