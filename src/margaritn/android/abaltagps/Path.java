package margaritn.android.abaltagps;

import android.location.Location;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.File;
import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.FileReader;
import android.util.Log;
import android.os.Environment;

// class to store location data and manage persistency (load/save to sd-card)
public class Path {
	protected ArrayList<Location> data;	
	protected String fileName;
	
	public Path(String fn) {
		fileName = Environment.getExternalStorageDirectory() + File.separator + fn;
		data = new ArrayList<Location>();
	}
	
	public void clear() {
		data.clear();
	}
	
	public Iterator<Location> iterator() {
		return data.iterator();
	}
	
	public void add(Location loc) {
		data.add(loc);
	}
	
	public void load() {
		String line;
		
		data.clear();
		try {
			BufferedReader br = new BufferedReader(new FileReader(fileName));
			while((line = br.readLine()) != null) {
				data.add(fromFileFormat(line));
			}
			br.close();
		} catch(Exception e) {
			Log.e(Constants.TAG, "file read failed", e);
			data.clear();
		}
	}
	
	public void save() {
		String line;

		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
			for(Location loc : data) {
				line = toFileFormat(loc);
				bw.write(line, 0, line.length());
				bw.newLine();				
			}
			bw.close();
		} catch(Exception e) {
			Log.e(Constants.TAG, "file write failed", e);
		}
	}
	
	protected Location fromFileFormat(String line) {
		Location loc = new Location(Constants.LOCATION_PROVIDER);
		String[] fields = line.split(Constants.FILE_FIELDS_DELIMITER);
		
		loc.setLongitude(Double.parseDouble(fields[0]));
		loc.setLatitude(Double.parseDouble(fields[1]));
		loc.setBearing(Float.parseFloat(fields[2]));
		loc.setSpeed(Float.parseFloat(fields[3]));
		loc.setAccuracy(Float.parseFloat(fields[4]));
		
		return loc;
	}
	
	protected String toFileFormat(Location loc) {
		String line = 
			String.valueOf(loc.getLongitude()) + Constants.FILE_FIELDS_DELIMITER +
			String.valueOf(loc.getLatitude()) + Constants.FILE_FIELDS_DELIMITER +
			String.valueOf(loc.getBearing()) + Constants.FILE_FIELDS_DELIMITER +
			String.valueOf(loc.getSpeed()) + Constants.FILE_FIELDS_DELIMITER +
			String.valueOf(loc.getAccuracy());
		return line;
	}	
}
