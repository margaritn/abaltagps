package margaritn.android.abaltagps;

import android.location.Location;
import android.location.LocationManager;
import android.location.LocationListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;

// class recording data from location provider /specified by Constants.LOCATION_PROVIDER/
class Recorder implements LocationListener {
	protected Displayer displayer;
	protected Path path;
	protected LocationManager lm;
	
	public Recorder(Path p, Displayer d) {
		this(p);
		this.displayer = d;
	}

	// "silent" recorder - just records gps data without displaying it
	public Recorder(Path p) {
		this.displayer = null;
		this.path = p;
		this.lm = (LocationManager)MainActivity.self.getSystemService(Context.LOCATION_SERVICE);		
	}
		
	public boolean start() {
		// check if required provider is enabled
		for(String provider : lm.getProviders(true)) { 
			if(provider.equals(Constants.LOCATION_PROVIDER)) {
				try {
					lm.requestLocationUpdates(Constants.LOCATION_PROVIDER, 
					Constants.RECORDING_PERIOD, 0.0f, this);
					path.clear(); // wipe out any previous recordings
					if(displayer != null) displayer.setStatus(MainActivity.i18n(R.string.waitingFix));
				} catch(Exception e) {
					Log.e(Constants.TAG, "lm.requestLocationUpdates failed", e); 
					return false;
				}
				return true; // provider enabled - updates started
			}
		}
		providerErrAlert();
		return false; // no such provider
	}
	
	protected void providerErrAlert() {
		new AlertDialog.Builder(MainActivity.self)
		.setTitle(Constants.LOCATION_PROVIDER + " " +	 MainActivity.i18n(R.string.providerNotFound))
		.setMessage(MainActivity.i18n(R.string.noGpsErr))
		.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() { 
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss(); 
			}})
		.show();
	}
	
	public void stop() {
		lm.removeUpdates(this);
		path.save();
		if(displayer != null) displayer.setStatus(MainActivity.i18n(R.string.stoppedStatus));
	}
	
	public void onLocationChanged(Location loc) {
		Log.d(Constants.TAG, "onLocationChanged: " + loc.toString());
		if(displayer != null) displayer.setLocation(loc);
		path.add(loc);
	}
	
	public void onProviderDisabled(String provider) {
		Log.d(Constants.TAG, (String)("onProviderDisabled:" + provider));
		providerErrAlert();
		// this way we are sure that 'click' action will be performed on the UI thread without an exception thrown
		MainActivity.self.runOnUiThread(new Runnable() {
			public void run() {	
				// cannot continue recording - provider disabled
				// simulate pressing bRecording btn to stop recording data
				MainActivity.self.findViewById(R.id.bRecording).performClick();
			}
		});
	}

    public void onProviderEnabled(String provider) {
        Log.d(Constants.TAG, (String)("onProviderEnabled:" + provider));
    }

    public void onStatusChanged(String provider, int n, Bundle bundle) {
        Log.d(Constants.TAG, "onStatusChanged: provider=" + provider + " int="
			+ String.valueOf(n) + " bundle=" + bundle.toString());
    }	
}

