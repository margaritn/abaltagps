package margaritn.android.abaltagps;

import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.util.Log;
import android.app.Activity;
import android.os.Bundle;

public class MainActivity extends Activity implements View.OnClickListener
{
	protected TextView vDisplay;
	protected Button bRecording;
	protected Button bPlayback;
	protected Recorder recorder;
	protected Player player;
	protected Path path;
	protected boolean recording, playing;

	public static Activity self;

    @Override
	public void onCreate(Bundle savedInstanceState)
	{
		Log.d(Constants.TAG, "MainActivity.onCreate");
		Log.d(Constants.TAG, getComponentName().toString());
		super.onCreate(savedInstanceState);
		self = this; // Context to be accessible outside MainActivity class
		setContentView(R.layout.main);
		bRecording = (Button)findViewById(R.id.bRecording);
		bRecording.setOnClickListener(this);
		bPlayback = (Button)findViewById(R.id.bPlayback);
		bPlayback.setOnClickListener(this);
		vDisplay = (TextView)findViewById(R.id.vDisplay);
		path = new Path(Constants.LOG_FILE_NAME);
		path.load(); //load previously recorded data, if any
		player = new Player(path, new Displayer(vDisplay, i18n(R.string.playbackStatus)));
		recorder = new Recorder(path, new Displayer(vDisplay, i18n(R.string.recordingStatus)));
		// app state, recording, playing, or idle
		recording = false;
		playing = false;
	}

	@Override
	protected void onDestroy()
	{
		Log.d(Constants.TAG, "MainActivity.onDestroy");
		if(recording) recorder.stop();
		if(playing) player.stop();
		super.onDestroy();
	}

	@Override
	public void onClick(View v)
	{
		Log.d(Constants.TAG, "MainActivity.onClick");
		
		switch(v.getId()) {
			case R.id.bRecording:
				if(recording) {
					recorder.stop();
					bPlayback.setEnabled(true);
				} else {
					if(!recorder.start()) break;
					bPlayback.setEnabled(false);
				}
				recording = !recording; // toggle start/stop
			break;
			
			case R.id.bPlayback:
				if(playing) player.stop();
				else if(!player.start()) break;
				bRecording.setEnabled(playing);
				playing = !playing; // toggle start/stop
			break;
			
			default:
		}
	}
	
	public static String i18n(int resId) {
		return MainActivity.self.getResources().getString(resId); // shorter form `self.getString(resId)`
	}
}

