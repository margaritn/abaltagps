package margaritn.android.abaltagps;

import java.util.Iterator;
import android.app.Activity;
import android.util.Log;
import android.os.CountDownTimer;
import android.location.Location;

// class implementing playback /using 'displayer' class/ of already recorded data
public class Player {
	protected Path path;
	protected Displayer displayer;
	protected CountDownTimer timer;
	protected Iterator<Location> iter;

	public Player(Path p, Displayer d) {
		path = p;
		displayer = d;
		timer = new CountDownTimer(Constants.PLAYBACK_PERIOD, Constants.PLAYBACK_PERIOD) {
			public void onFinish() {
				if(iter.hasNext()) {
					displayer.setLocation(iter.next());
					timer.start();
				} else {
					// this way we are sure that 'click' action will be performed on the UI thread without an exception thrown
					MainActivity.self.runOnUiThread(new Runnable() {
						public void run() {
							MainActivity.self.findViewById(R.id.bPlayback).performClick();
						}
					});
				}
			}
			public void onTick(long l) {}
		};
	}
	
	public boolean start() {
		iter = path.iterator();
		if(iter.hasNext()) {
			displayer.setLocation(iter.next());
			timer.start();
			return true;
		} else {
			return false;
		}
	}
	
	public void stop() {
		timer.cancel();
		displayer.setStatus(MainActivity.i18n(R.string.stoppedStatus));	
	}
}

